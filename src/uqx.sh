#!/bin/dash
IN_SESSDIR='/usr/share/xsessions';
OUT_SESSDIR='/etc/qingy/xsessions';

FILES=$(ls ${IN_SESSDIR}/*.desktop);

for i in ${FILES}; do
	FILE=${i};

	SESSNAME=$(grep -i ^'name=' ${FILE} | cut -d '=' -f2 | tr '/' '-' | tr [:blank:] '_');
	STARTUPCMD=$(grep -i ^'exec=' ${FILE} | cut -d '=' -f2);

	echo Found name: ${SESSNAME} for ${FILE};
	echo Found Startcmd: ${STARTUPCMD} for ${FILE};

	echo '#!/bin/dash' > ${OUT_SESSDIR}/${SESSNAME};
	echo '. /etc/profile' >> ${OUT_SESSDIR}/${SESSNAME};
	echo ${STARTUPCMD} >> ${OUT_SESSDIR}/${SESSNAME};
done;